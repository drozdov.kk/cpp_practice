#include <boost/test/auto_unit_test.hpp>
#include <stdexcept>
#include "composite-shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

const double ACCURACY = 0.001;

BOOST_AUTO_TEST_SUITE(A3TestCompositeShape)

  BOOST_AUTO_TEST_CASE(testConstructors)
  {
    drozdov::CompositeShape emptyComposite;
    const int zeroQty = 0;
    BOOST_CHECK_EQUAL(emptyComposite.getQuantity(), zeroQty);

    drozdov::Circle circle(4.0, {2.3, 4.0});
    drozdov::CompositeShape composite1(circle);
    const int oneQty = 1;
    BOOST_CHECK_EQUAL(composite1.getQuantity(), oneQty);

    drozdov::CompositeShape composite2(composite1);
    BOOST_CHECK_EQUAL(composite1.getQuantity(), composite2.getQuantity());

    const int qtyBeforeMove = composite2.getQuantity();
    drozdov::CompositeShape composite3(std::move(composite2));
    BOOST_CHECK_EQUAL(composite2.getQuantity(), zeroQty);
    BOOST_CHECK_EQUAL(composite3.getQuantity(), qtyBeforeMove);
  } //testConstructors

  BOOST_AUTO_TEST_CASE(testAssigmentOperators)
  {
    drozdov::CompositeShape emptyComposite;
    drozdov::Circle circle(2.0, {-1.5, 2.0});
    const int zeroQty = 0;

    drozdov::CompositeShape composite(circle);
    drozdov::Rectangle rectangle(3.0, 4.0, {2.5, 3.5});
    composite.addShape(rectangle);
    const int qtyBeforeCopy = composite.getQuantity();

    emptyComposite = composite;

    BOOST_CHECK_EQUAL(emptyComposite.getQuantity(), composite.getQuantity());
    BOOST_CHECK_EQUAL(qtyBeforeCopy, composite.getQuantity());

    const int qtyBeforeRemove = emptyComposite.getQuantity();

    composite = std::move(emptyComposite);

    BOOST_CHECK_EQUAL(emptyComposite.getQuantity(), zeroQty);
    BOOST_CHECK_EQUAL(composite.getQuantity(), qtyBeforeRemove);
  } // testAssigmentOperators

  BOOST_AUTO_TEST_CASE(testAddAndRemove)
  {
    drozdov::CompositeShape composite;

    drozdov::Circle circle1(4.0, {2.3, 4.0});
    composite.addShape(circle1);
    BOOST_CHECK_EQUAL(composite[1], &circle1);

    drozdov::Rectangle rectangle(3.0, 4.0, {2.5, 3.5});
    composite += rectangle;
    BOOST_CHECK_EQUAL(composite[2], &rectangle);

    drozdov::Circle circle2(2.0, {-1.5, 2.0});
    composite = composite + circle2;
    BOOST_CHECK_EQUAL(composite[3], &circle2);

    const int qtyBeforeRemove = composite.getQuantity();
    composite.removeShape(2);
    BOOST_CHECK_EQUAL(composite.getQuantity(), qtyBeforeRemove - 1);
    BOOST_CHECK_EQUAL(composite[1], &circle1);
    BOOST_CHECK_EQUAL(composite[2], &circle2);
  } //testAddAndRemove

BOOST_AUTO_TEST_SUITE_END() //A3TestCompositeShape

BOOST_AUTO_TEST_SUITE(A3TestInheritedMethods)

  BOOST_AUTO_TEST_CASE(testCorrectnessScale)
  {
    drozdov::CompositeShape composite;
    drozdov::Circle circle(4.0, {2.3, 4.0});
    drozdov::Rectangle rectangle(3.0, 4.0, {2.5, 3.5});
    composite.addShape(circle);
    composite.addShape(rectangle);

    const double areaBeforeScale = composite.getArea();
    const double coefficientScale = 3.0;
    composite.scale(coefficientScale);
    const double areaAfterScale = composite.getArea();

    BOOST_CHECK_CLOSE(areaBeforeScale * coefficientScale * coefficientScale, areaAfterScale, ACCURACY);
  } //testCorrectnessScale

  BOOST_AUTO_TEST_CASE(testCorrectnessMove)
  {
    drozdov::CompositeShape composite;
    drozdov::Circle circle(4.0, {2.3, 4.0});
    drozdov::Rectangle rectangle(3.0, 4.0, {2.5, 3.5});
    composite.addShape(circle);
    composite.addShape(rectangle);

    const double areaBeforeMove = composite.getArea();
    const drozdov::rectangle_t frameBeforeMove = composite.getFrameRect();

    composite.move(2.4, 5.6);
    const drozdov::rectangle_t frameAfterMoveOn = composite.getFrameRect();
    const double areaAfterMoveOn = composite.getArea();

    BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMoveOn, ACCURACY);
    BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMoveOn.width, ACCURACY);
    BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMoveOn.height, ACCURACY);

    composite.move({2.4, 5.6});
    const drozdov::rectangle_t frameAfterMoveTo = composite.getFrameRect();
    const double areaAfterMoveTo = composite.getArea();

    BOOST_CHECK_CLOSE(areaBeforeMove, areaAfterMoveTo, ACCURACY);
    BOOST_CHECK_CLOSE(frameBeforeMove.width, frameAfterMoveTo.width, ACCURACY);
    BOOST_CHECK_CLOSE(frameBeforeMove.height, frameAfterMoveTo.height, ACCURACY);
  } //testCorrectnessMove

BOOST_AUTO_TEST_SUITE_END() //A3TestInheritedMethods

BOOST_AUTO_TEST_SUITE(A3TestCorrectnessThrowMethods)

  BOOST_AUTO_TEST_CASE(testLogicErrorEmptyCompositeShape)
  {
    drozdov::CompositeShape emptyComposite;
    BOOST_CHECK_THROW(emptyComposite[0]->getArea(), std::logic_error);
    BOOST_CHECK_THROW(emptyComposite.getArea(), std::logic_error);
    BOOST_CHECK_THROW(emptyComposite.getPos(), std::logic_error);
    BOOST_CHECK_THROW(emptyComposite.getFrameRect(), std::logic_error);
    BOOST_CHECK_THROW(emptyComposite.scale(10.0), std::logic_error);
    BOOST_CHECK_THROW(emptyComposite.move(1.0, 2.0), std::logic_error);
    BOOST_CHECK_THROW(emptyComposite.move({1.0, 2.0}), std::logic_error);
  } //testLogicErrorEmptyCompositeShape

  BOOST_AUTO_TEST_CASE(testLogicErrorWhenAddShapes)
  {
    drozdov::Circle circle(4.0, {2.3, 4.0});
    drozdov::CompositeShape composite(circle);
    BOOST_CHECK_THROW(composite.addShape(circle), std::logic_error);
    BOOST_CHECK_THROW(composite += circle, std::logic_error);
    BOOST_CHECK_THROW(composite = composite + circle, std::logic_error);
  } //testLogicErrorWhenAddShapes

  BOOST_AUTO_TEST_CASE(testOutOfRange)
  {
    drozdov::Circle circle(4.0, {2.3, 4.0});
    drozdov::CompositeShape composite(circle);
    BOOST_CHECK_THROW(composite[2]->getArea(), std::out_of_range);
    BOOST_CHECK_THROW(composite.removeShape(2), std::out_of_range);
  } //ttestOutOfRange

  BOOST_AUTO_TEST_CASE(testInvalidArgument)
  {
    const double invalidArgument = -10.0;
    drozdov::Circle circle(4.0, {2.3, 4.0});
    drozdov::CompositeShape composite(circle);
    BOOST_CHECK_THROW(composite.scale(invalidArgument), std::invalid_argument);
  } //testInvalidArgument

BOOST_AUTO_TEST_SUITE_END() //A3TestCorrectnessThrowMethods
