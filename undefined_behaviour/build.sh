#!/bin/bash

echo Компилируем, обращая внимание на предупреждения!
g++ -o main -g --std=c++14 -Wall -Wextra -Wold-style-cast main.cpp

echo Запускаем
./main
