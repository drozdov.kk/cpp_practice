# Области IT

1. Безопасность, cybersecurity
2. Связь (5G)
3. Социальные сети, сервисы, мессенджеры
4. Web-программирование (front end, back end, full stack)
5. Искусственный интеллект (AI) и машинное обучение (ML)
6. Интернет вещей
7. Облачные вычисления (в том числе, serverless, например Amazon Lambda)
8. Блокчейн и криптовалюта
9. Финансы
10. Биоинформатика
11. Встроенные системы
12. Беспилотные средства передвижения
13. Виртуализация
14. Computer Aided Design (CAD)
15. Автоматизация научного эксперимента и измерений
16. Медицина (насущные нужды)
17. Фармацевтика (новые лекарства)
18. Документооборот: юридический, медицинский, научный, бухгалтерский и т.д.
19. Квантовые компьютеры (Q#)
20. Разработка компиляторов
21. Автоматизация программирования (Computer Aided Software Engineering - CASE)
22. Космонавтика
23. Робототехника
24. Big Data (работа с данными огромного объема)
25. Базы данных
26. Аналитика
27. UI/UX
28. Игры разного назначения
29. Обработка звука, видео, мультимедиа
30. Потоковое вещание
31. Умный дом
32. Умный город
33. Чрезвычайные службы (полиция, МЧС, армия)
34. Экология
35. Экономика
36. Мобильные приложения
37. Разработка операционных систем
38. Высоконагруженные системы
39. Алгоритмы
40. Научные исследования и образование